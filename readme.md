# Invoices (Real Estate Invoice Management System)

The specified system is developed to manage the invoices of a business specifically belongs to the real estate business domain(The methodology known as Domain Driven Design). We have managed the properties, units (leased) and invoices, as well as handled the response time in an optimised way to give the response to multiple clients in an efficient way.

The system is developed using the REST architecture.You may check the endpoints information below. See the technology stack below under the section Build With.

## Getting Started

Clone the project on your machine for development and testing purposes. See the notes to learn how to deploy the system on an environment.

### Prerequisites

You need to install the followings on your machine in order to setup the development environment on your  machine.

```
PHP >=7.1
```
Install the [composer](https://getcomposer.org/) The dependency manager on your machine to install the project dependencies.
```
PHP composer
```
Install the git on your machine
```
git
```
### Installing

Clone the project using the command below.

```
$ git clone https://bitbucket.org/seabdulsamad/invoices.git
```

Install project dependencies by running this command in the project's root directory:

```
$ composer install
```
If you change structure, paths, namespaces, etc., make sure you run the [autoload generator](https://getcomposer.org/doc/03-cli.md#dump-autoload):
```sh
$ composer dump-autoload
```
Set the environment variables for the database connectivity in .env file as we are using the MYSQL databse. For Example
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=base64:02YKP88xw3PjLmNkQcJuena8VJQfVBFy4zY1mPR4qbY=
APP_TIMEZONE=UTC

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=invoices
DB_USERNAME=root
DB_PASSWORD=

CACHE_DRIVER=file
QUEUE_DRIVER=sync
```
Run any of the artisan command for the database schema creation. You may use bin/vendors instead of artisan in below commands depends upon your environment.
```sh
$ php artisan doctrine:schema:create
$ php artisan migrate
```
Run the artisan command to seed the database tables with some test data.Although doctrine provides the facility to write the fixtures to fill the dummy data into schemas but I am considering that It is the quickest way to do so at the moment.
```sh
$ php artisan db:seed
```
## Running the tests

To run the tests:
```
$ php vendor/phpunit/phpunit/phpunit
```

## Built With

* [Lumen](https://lumen.laravel.com/docs/5.6) - The stunningly fast micro-framework by Laravel.
* [Composer](https://getcomposer.org/doc/) - Dependency Management
* [MySQL](https://www.mysql.com/) - Ann open-source relational database management system
* [Doctrine](https://laraveldoctrine.org/docs/current/orm/lumen) - An object-relational mapper (ORM) for PHP

## Assumptions

* Originally the Laravel was required for the system. But I am assuming that the configurations are almost same for that and we are not needing any sort of UI at the moment so I am using the Lumen to get the JSON response.
* As we just need the retrieval of data from the system at the moment so I just implemented the Query from the CRUD options.

## Endpoint

* **URL**
```
http://site-url.com/properties
```
## Authors

* **Abdul Samad** - se.abdulsamad@gmail.com

## License

This project is licensed under the MIT License [Abdul Samad](http://linkedin.com/in/abdul-samad-993b8450)