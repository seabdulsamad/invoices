<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Property
 *
 * @ORM\Table(name="property")
 * @ORM\Entity(repositoryClass="App\Repository\PropertyRepository")
 */
class Property
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="property_name", type="string", length=255, nullable=false)
     */
    private $propertyName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="no_of_units", type="integer", nullable=false)
     */
    private $noOfUnits;

    /**
     * @var int
     *
     * @ORM\Column(name="vacant_units", type="integer", nullable=false)
     */
    private $vacantUnits;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255, nullable=false)
     */
    private $area;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    /**
     * @param string $propertyName
     * @return Property
     */
    public function setPropertyName(string $propertyName): Property
    {
        $this->propertyName = $propertyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Property
     */
    public function setDescription(string $description): Property
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getNoOfUnits(): int
    {
        return $this->noOfUnits;
    }

    /**
     * @param int $noOfUnits
     * @return Property
     */
    public function setNoOfUnits(int $noOfUnits): Property
    {
        $this->noOfUnits = $noOfUnits;
        return $this;
    }

    /**
     * @return int
     */
    public function getVacantUnits(): int
    {
        return $this->vacantUnits;
    }

    /**
     * @param int $vacantUnits
     * @return Property
     */
    public function setVacantUnits(int $vacantUnits): Property
    {
        $this->vacantUnits = $vacantUnits;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Property
     */
    public function setCountry(string $country): Property
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea(): string
    {
        return $this->area;
    }

    /**
     * @param string $area
     * @return Property
     */
    public function setArea(string $area): Property
    {
        $this->area = $area;
        return $this;
    }

}
