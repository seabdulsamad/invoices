<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoices
 *
 * @ORM\Table(name="invoices", indexes={@ORM\Index(name="invoices_property_id_foreign", columns={"property_id"}), @ORM\Index(name="invoices_unit_id_foreign", columns={"unit_id"})})
 * @ORM\Entity
 */
class Invoices
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_mode", type="string", length=255, nullable=false)
     */
    private $paymentMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date", type="date", nullable=false)
     */
    private $paymentDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Property
     *
     * @ORM\ManyToOne(targetEntity="Property")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     * })
     */
    private $property;

    /**
     * @var \Units
     *
     * @ORM\ManyToOne(targetEntity="Units")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     * })
     */
    private $unit;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Invoices
     */
    public function setAmount(float $amount): Invoices
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMode(): string
    {
        return $this->paymentMode;
    }

    /**
     * @param string $paymentMode
     * @return Invoices
     */
    public function setPaymentMode(string $paymentMode): Invoices
    {
        $this->paymentMode = $paymentMode;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaymentDate(): \DateTime
    {
        return $this->paymentDate;
    }

    /**
     * @param \DateTime $paymentDate
     * @return Invoices
     */
    public function setPaymentDate(\DateTime $paymentDate): Invoices
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * @return \Property
     */
    public function getProperty(): \Property
    {
        return $this->property;
    }

    /**
     * @param \Property $property
     * @return Invoices
     */
    public function setProperty(\Property $property): Invoices
    {
        $this->property = $property;
        return $this;
    }

    /**
     * @return \Units
     */
    public function getUnit(): \Units
    {
        return $this->unit;
    }

    /**
     * @param \Units $unit
     * @return Invoices
     */
    public function setUnit(\Units $unit): Invoices
    {
        $this->unit = $unit;
        return $this;
    }

}
