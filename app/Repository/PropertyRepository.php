<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PropertyRepository extends EntityRepository{

    /**
     * @return mixed
     */
    public function getPropertyInvoices()
    {
        /*
         * The SQL query generation using the method createQuery
         */
        $query = /** @lang sql */
        'SELECT p.propertyName, p.noOfUnits,p.vacantUnits,p.country,p.area,
        (p.noOfUnits-p.vacantUnits) AS occupiedUnites,
        (SELECT SUM(offline.amount) 
        FROM App\Entities\Invoices offline 
        WHERE offline.paymentMode=:offlineMode AND offline.property=p.id) AS offlineAmount,
        (SELECT SUM(online.amount) 
        FROM App\Entities\Invoices online 
        WHERE online.paymentMode=:onlineMode  AND online.property=p.id) AS onlineAmount
        FROM App\Entities\Property AS p';
        return $this->_em->createQuery($query)
            ->setParameters(array('offlineMode'=>'offline','onlineMode'=>'online'))
            ->getArrayResult();
        /*
         * Optional: An other way to generate the query using createQueryBuilder method
         * the query is same but there are different ways to generate the query provided by the EntityManager
         */
        $qb = $this->_em->createQueryBuilder();
        return $qb->select('p.propertyName, p.noOfUnits,p.vacantUnits,p.country,p.area')
            ->addSelect('(p.noOfUnits-p.vacantUnits) AS occupiedUnites')
            ->addSelect('(SELECT SUM(i.amount) FROM App\Entities\Invoices i 
            WHERE i.paymentMode=:offlineMode AND i.property=p.id) AS offlineAmount')
            ->addSelect('(SELECT SUM(onl.amount) FROM App\Entities\Invoices onl 
            WHERE onl.paymentMode=:onlineMode  AND onl.property=p.id) AS onlineAmount')
            ->from('App\Entities\Property','p')
            ->getQuery()
            ->setParameters(array('offlineMode'=>'offline','onlineMode'=>'online'))
            ->getArrayResult();
    }
}