<?php

namespace App\Http\Controllers;
use App\Entities\Invoices;
use App\Entities\Property;
use App\Entities\Units;
use Doctrine\ORM\EntityManagerInterface;
use Laravel\Lumen\Routing\Controller;

class PropertiesController extends Controller
{

    /**
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function index(EntityManagerInterface $em)
    {
        $propertyInfo = $em->getRepository('App\Entities\Property')->getPropertyInvoices();
        return $propertyInfo;
    }
}