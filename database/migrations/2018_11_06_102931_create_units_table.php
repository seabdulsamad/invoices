<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('code',255); //Human-Readable code for each unit
            $table->string('description',500);
            $table->double('area'); //Calculate in SQFT
            $table->string('owner_name',255);
            $table->string('owner_phone',255);
            $table->integer('property_id')->unsigned()->nullable();
            $table->enum('status',['vacant','lease','rent']);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
        });

        Schema::table('units', function($table) {
            $table->foreign('property_id')->references('id')->on('property');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
