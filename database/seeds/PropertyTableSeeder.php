<?php

use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('property')->insert(
            array(
                array(
                    'id' => '1',
                    'property_name' => 'Marina Plaza',
                    'description' => 'Marina Plaza',
                    'no_of_units' => '2',
                    'vacant_units' => '0',
                    'country' => 'United Arab Emirates',
                    'area' => 'Dubai Marina',
                    'created_at' => '2018-11-06 00:00:00',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '2',
                    'property_name' => 'Eiffel Accommodation  ',
                    'description' => 'Eiffel Accommodation  ',
                    'no_of_units' => '3',
                    'vacant_units' => '1',
                    'country' => 'United Arab Emirates',
                    'area' => 'Al Qouz Dubai',
                    'created_at' => '2018-11-05 00:00:00',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '3',
                    'property_name' => 'Al Fahad',
                    'description' => 'Al Fahad',
                    'no_of_units' => '1',
                    'vacant_units' => '0',
                    'country' => 'United Arab Emirates',
                    'area' => 'Tecom Dubai',
                    'created_at' => '2018-11-04 00:00:00',
                    'updated_at' => NULL
                )
            )
        );

    }
}
