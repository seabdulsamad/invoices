<?php

use Illuminate\Database\Seeder;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->insert(
            array(
                array(
                    'id' => '1',
                    'property_id' => '1',
                    'unit_id' => '1',
                    'amount' => '2500',
                    'payment_mode' => 'online',
                    'payment_date' => '2018-05-04',
                    'created_at' => '2018-11-06 11:30:47',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '2',
                    'property_id' => '1',
                    'unit_id' => '2',
                    'amount' => '2850',
                    'payment_mode' => 'offline',
                    'payment_date' => '2018-11-06',
                    'created_at' => '2018-11-06 04:32:36',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '3',
                    'property_id' => '2',
                    'unit_id' => '3',
                    'amount' => '3200',
                    'payment_mode' => 'offline',
                    'payment_date' => '2018-11-07',
                    'created_at' => '2018-11-04 00:00:00',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '4',
                    'property_id' => '1',
                    'unit_id' => '1',
                    'amount' => '3550',
                    'payment_mode' => 'online',
                    'payment_date' => '2015-05-04',
                    'created_at' => '2018-11-06 11:30:47',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '5',
                    'property_id' => '2',
                    'unit_id' => '3',
                    'amount' => '4400',
                    'payment_mode' => 'offline',
                    'payment_date' => '2018-11-06',
                    'created_at' => '2018-11-06 11:30:47',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '6',
                    'property_id' => '2',
                    'unit_id' => '4',
                    'amount' => '4500',
                    'payment_mode' => 'offline',
                    'payment_date' => '2018-05-04',
                    'created_at' => '2018-11-06 11:30:47',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '7',
                    'property_id' => '2',
                    'unit_id' => '4',
                    'amount' => '1800',
                    'payment_mode' => 'offline',
                    'payment_date' => '2017-11-06',
                    'created_at' => '2017-06-22 05:15:23',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '8',
                    'property_id' => '3',
                    'unit_id' => '6',
                    'amount' => '13000',
                    'payment_mode' => 'online',
                    'payment_date' => '2015-02-05',
                    'created_at' => '2015-03-06 03:08:24',
                    'updated_at' => NULL)
            ,
                array(
                    'id' => '9',
                    'property_id' => '3',
                    'unit_id' => '6',
                    'amount' => '13000',
                    'payment_mode' => 'online',
                    'payment_date' => '2016-02-05',
                    'created_at' => '2016-03-06 03:08:24',
                    'updated_at' => NULL
                )
            )
        );
    }
}
