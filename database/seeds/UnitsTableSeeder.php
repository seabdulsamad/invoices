<?php

use Illuminate\Database\Seeder;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert(
            array(
                array(
                    'id' => '1',
                    'title' => 'Apartment MP-1806',
                    'code' => 'MP-1806',
                    'description' => 'Apartment MP-1806',
                    'area' => '5874',
                    'owner_name' => 'Mohammad Emman',
                    'owner_phone' => '058987458',
                    'property_id' => '1',
                    'status' => 'lease',
                    'start_date' => '2017-11-06',
                    'end_date' => '2019-11-06',
                    'created_at' => '2018-11-06 04:24:11',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '2',
                    'title' => 'Studio MP-2503',
                    'code' => 'MP-2503',
                    'description' => 'Studio MP-2503',
                    'area' => '2500',
                    'owner_name' => 'Mohammad Ali',
                    'owner_phone' => '053256897',
                    'property_id' => '1',
                    'status' => 'rent',
                    'start_date' => '2018-02-05',
                    'end_date' => '2019-02-04',
                    'created_at' => '2018-11-06 04:42:21',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '3',
                    'title' => 'Apartment EA-202',
                    'code' => 'EA-202',
                    'description' => 'Apartment EA-202',
                    'area' => '3548',
                    'owner_name' => 'Mohammad Rizwan',
                    'owner_phone' => '0521457895',
                    'property_id' => '2',
                    'status' => 'lease',
                    'start_date' => '2016-11-06',
                    'end_date' => '2019-11-06',
                    'created_at' => '2016-11-06 00:00:00',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '4',
                    'title' => '2BHK EA-105',
                    'code' => 'EA-2BHK-105',
                    'description' => '2BHK EA-105 Apartment',
                    'area' => '5680',
                    'owner_name' => 'Emma',
                    'owner_phone' => '0525478982',
                    'property_id' => '2',
                    'status' => 'lease',
                    'start_date' => '2017-11-06',
                    'end_date' => '2022-11-21',
                    'created_at' => '2017-11-06 05:14:23',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '5',
                    'title' => 'Studio EA-109',
                    'code' => 'EA-109',
                    'description' => 'Studio EA-109',
                    'area' => '3564',
                    'owner_name' => 'Hiba Sabra',
                    'owner_phone' => '0569878965',
                    'property_id' => '2',
                    'status' => 'vacant',
                    'start_date' => '0000-00-00',
                    'end_date' => '0000-00-00',
                    'created_at' => '2018-11-06 07:12:25',
                    'updated_at' => NULL
                ),
                array(
                    'id' => '6',
                    'title' => '3BHK Apartment F1-1809',
                    'code' => 'F1-1809',
                    'description' => '3BHK Apartment F1-1809',
                    'area' => '8970',
                    'owner_name' => 'Abdul Samad',
                    'owner_phone' => '0524219320',
                    'property_id' => '3',
                    'status' => 'lease',
                    'start_date' => '2017-11-06',
                    'end_date' => '2020-11-06',
                    'created_at' => '2015-11-06 06:36:18',
                    'updated_at' => NULL
                )
            )
        );
    }
}
